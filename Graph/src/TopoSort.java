import java.util.ArrayList;

public class TopoSort {

	
	static Graph g;
	static boolean[] verts;
	
	public static void connected(Graph ga){
		
		g = ga;
		verts = new boolean[ga.getVertices().length];
		
		for(int j = 0; j<ga.getVertices().length; j++){
			if(!check(j)){
				dfs(j);
			}
		}
		
	}
	
	private static void dfs(int i){
		
		verts[i]=true;
		ArrayList<Edge> edgy = g.getVertices()[i].getEdges();
		for(Edge e:edgy){
			if(!check(e.getToVertex().getNumber())){
				dfs(e.getToVertex().getNumber());
			}
		}
		System.out.println("" + i + " ");
	}
	
	private static boolean check(int i){
		return verts[i];
	}
	
}
