import java.util.ArrayList;

public class DFS {

	private DFS(){}
	
	static Graph g;
	static boolean[] verts;
	
	public static boolean connected(Graph ga, int i){
		
		g = ga;
		verts = new boolean[ga.getVertices().length];
		
		dfs(i);
		
		for(boolean boo:verts){
			if(!boo){
				return false;
			}
		}
		return true;
	}
	
	private static void dfs(int i){
		verts[i]=true;
		ArrayList<Edge> edgy = g.getVertices()[i].getEdges();
		for(Edge e:edgy){
			if(!check(e.getToVertex().getNumber())){
				dfs(e.getToVertex().getNumber());
			}
		}
	}
	
	private static boolean check(int i){
		return verts[i];
	}
	
	
}
