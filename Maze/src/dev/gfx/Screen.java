package dev.gfx;

import dev.Settings;

public class Screen {

	private int width, height;


	public Screen(int width) {
		this.height = width;
		this.width = width;
		init();
	}

	public void init() {
		StdDraw.setCanvasSize(width, height);
		StdDraw.setXscale(-Settings.tileWidth, Settings.width);
		StdDraw.setYscale(-Settings.tileWidth, Settings.height);

		StdDraw.enableDoubleBuffering();

		show();
	}

	public void show(){

		StdDraw.show();
		StdDraw.clear();
	}
}
