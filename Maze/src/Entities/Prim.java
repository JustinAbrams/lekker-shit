package Entities;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Stack;

import dev.Maze;
import dev.Settings;
import dev.gfx.StdDraw;
import dev.tile.Tile;

public class Prim {

	private int x, y;

	private Stack<int[]> passed;

	private float xp, yp, halfTile;

	public Prim(int x, int y){
		this.x = x;
		this.y = y;

		passed = new Stack<int[]>();

		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
		halfTile = Settings.tileWidth/2;

	}

	public void render(){
		StdDraw.setPenColor(new Color(0, 64, 255));
		StdDraw.filledSquare(xp, yp, halfTile-halfTile/5);
	}

	public void tick(Maze m){

		m.getTile(x, y).visit(true);

		move(m);

		xp = x*Settings.tileWidth;
		yp = y*Settings.tileWidth;
	}

	public void move(Maze m) {
		Tile[] neighbours = m.getNeighbours(x, y);
		ArrayList<Tile> next =  new ArrayList<Tile>();

		for (int i =0; i<4; i++) {
			if(neighbours[i]!=null && !neighbours[i].isVisited()) {
				next.add(neighbours[i]);
			}
		}

		if(next.size()==0){
			if(passed.size()>0){
				int[] pos = passed.pop();
				x = pos[0];
				y = pos[1];
			}
		} else {
			int[] pos = new int[2]; pos[0] = x; pos[1] = y;
			passed.push(pos);
			int r =((int)(Math.random()*next.size()));
			x = next.get(r).getX();
			y = next.get(r).getY();
		
			//BreakWalls
			pos = passed.peek();
			m.breakWalls(pos[0], pos[1], x, y);
		}
	}
	
	public boolean isFinished() {
		return (passed.size()<1);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}